@tool
extends Sprite2D

# This is your own script code

@export var go_run_my_fancy_func:String

#To capture the button press, add the connected signal func:
func _on_button_pressed(text:String):
	if text.to_upper() == "PRESS TO RUN MY FANCY FUNC":
		my_fancy_func()
		
func my_fancy_func():
	print("yo!")
